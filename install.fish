#!/usr/bin/env fish

set -l files \
    .config/fish \
    .config/nvim \
    .config/tmux \
    .haskeline \
    .inputrc \
    .ipython \
    .local/share/fortunes \

for name in $files
    if test -L ~/$name
        continue
    end
    if test -e ~/$name
        echo ~/$name already exists
        continue
    end
    mkdir -p (dirname ~/$name); or continue
    ln -srv ~/.dotfiles/$name ~/$name
end
