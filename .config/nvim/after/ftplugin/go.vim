augroup go
	autocmd!
	au BufWritePre *.go call Fmt('gofmt')
augroup END
