lua << EOF
vim.lsp.start({
	name = "rust-analyzer",
	cmd = { "rust-analyzer" },
	root_dir = vim.fs.dirname(vim.fs.find("Cargo.toml", {
		upward = true,
		stop = vim.loop.os_homedir(),
		path = vim.fs.dirname(vim.api.nvim_buf_get_name(0)),
	})[1]),
})
EOF

augroup rust
	autocmd!
	autocmd BufWritePre *.rs  lua vim.lsp.buf.format()
augroup END

autocmd LspAttach <buffer>  nnoremap K :lua vim.lsp.buf.hover()<CR>

set formatoptions-=o
