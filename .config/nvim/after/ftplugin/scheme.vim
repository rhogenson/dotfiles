set autoindent
set lisp
set tabstop=2
set shiftwidth=2
set expandtab
set formatoptions+=j
