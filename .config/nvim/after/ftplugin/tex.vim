set spell
set textwidth=72
map <F2> :w<CR>:!latexmk -pdf -latexoption=-shell-escape %<CR>
map <F3> :!mupdf-x11 $(echo % \| sed s/tex/pdf/) &<CR>
filetype plugin indent off
set autoindent
set autochdir
