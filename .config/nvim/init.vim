set mouse=
set guicursor=
set undofile
set softtabstop=-1

set number
set relativenumber

set nohlsearch
colorscheme torte
