function trash
    set -l trash ~/.local/share/Trash
    if set -q XDG_DATA_HOME
        set trash "/$XDG_DATA_HOME/Trash"
    end
    mkdir -p -- $trash/{files,info}; or return

    set -l now (date +%FT%T)
    for f in $argv
        set -l path (realpath -zs -- $f | string split0); or continue
        set -l info (mktemp -- $trash/info/(basename -z -- $f | string split0).$now.XXXXXXXXXX.trashinfo | string collect); or continue
        echo "[Trash Info]
Path=$(string escape --style=url -- $path)
DeletionDate=$now" > $info; or continue
        mv -- $f $trash/files/(basename -z -s .trashinfo -- $info | string split0); or rm -- $info
    end
end
