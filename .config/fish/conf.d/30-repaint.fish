if not status is-interactive
    exit
end

function __sigwinch_repaint --on-signal SIGWINCH
    commandline -f repaint
end
