if not status is-interactive
    exit
end

# Let the terminal handle text reflow.
# https://fishshell.com/docs/current/language.html#envvar-fish_handle_reflow
set -g fish_handle_reflow 0
